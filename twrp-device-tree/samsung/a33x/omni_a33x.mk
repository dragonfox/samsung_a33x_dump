#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from a33x device
$(call inherit-product, device/samsung/a33x/device.mk)

PRODUCT_DEVICE := a33x
PRODUCT_NAME := omni_a33x
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-A336M
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="a33xub-user 12 SP1A.210812.016 A336MUBS7CWJ1 release-keys"

BUILD_FINGERPRINT := samsung/a33xub/a33x:12/SP1A.210812.016/A336MUBS7CWJ1:user/release-keys
